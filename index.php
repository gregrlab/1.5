

<?php

$jsonFile = file_get_contents(__DIR__ . '/contacts.json');
$contacts = json_decode($jsonFile, true);
?>

<table>
    <tr>
        <td>Имя</td>
        <td>Фамилия</td>
        <td>Номер телефона</td>
        <td>Адрес</td>
    </tr>
    <?php foreach ($contacts as $contact) { ?>
    <tr>
        <td><?php echo $contact['firstName']; ?></td>
        <td><?php echo $contact['lastName']; ?></td>
        <td><?php echo $contact['phoneNumber']; ?></td>
        <td><?php echo $contact['address']; ?></td>
    </tr>
   <?php } ?>

</table>